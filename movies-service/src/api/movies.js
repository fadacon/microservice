'use strict'
const status = require('http-status')

module.exports = (app, options) => {
  const {repo,eureka} = options

  app.get('/movies', (req, res, next) => {
    repo.getAllMovies().then(movies => {
      console.log(eureka.getInstancesByAppId('booking-service'));
      res.status(status.OK).json(eureka.getInstancesByAppId('booking-service'))
    }).catch(next)
  })

  app.get('/movies/premieres', (req, res, next) => {
    repo.getMoviePremiers().then(movies => {
      res.status(status.OK).json(movies)
    }).catch(next)
  })

  app.get('/movies/:id', (req, res, next) => {
    repo.getMovieById(req.params.id).then(movie => {
      res.status(status.OK).json(movie)
    }).catch(next)
  })
}
