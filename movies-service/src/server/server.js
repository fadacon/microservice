const express = require('express')
const morgan = require('morgan')
const helmet = require('helmet')
const api = require('../api/movies')

const start = (options) => {
    return new Promise((resolve, reject) => {


        console.log('vao');
        const Eureka = require('eureka-js-client').Eureka;

        const client = new Eureka({
            // application instance information
            instance: {
                app: 'movie-service',
                hostName: 'localhost',
                ipAddr: '127.0.0.1',
                port: {
                    '$': options.port,
                    '@enabled': true,
                },
                vipAddress: 'movies.test.something.com',
                dataCenterInfo: {
                    '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
                    name: 'MyOwn',
                },
            },
            eureka: {
                // eureka server host / port
                host: '127.0.0.1',
                port: 8080,
                servicePath: '/eureka-server-1.8.8/v2/apps'
            },
        });
        client.logger.level('debug');
        console.log('running eureka');
        client.start(function (error) {
            if (!options.repo) {
                reject(new Error('The server must be started with a connected repository'))
            }
            if (!options.port) {
                reject(new Error('The server must be started with an available port'))
            }

            const app = express()
            app.use(morgan('dev'))
            app.use(helmet())
            app.use((err, req, res, next) => {
                reject(new Error('Something went wrong!, err:' + err))
                res.status(500).send('Something went wrong!')
            })

            console.log(error || 'complete');
            console.log(client.getInstancesByAppId('booking-service'));
            options.eureka = client;
            api(app, options);
            const server = app.listen(options.port, () => resolve(server))

        });
    })
}

module.exports = Object.assign({}, {start})
