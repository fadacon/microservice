const express = require('express')
const morgan = require('morgan')
const helmet = require('helmet')
const bodyparser = require('body-parser')
const cors = require('cors')
const _api = require('../api/booking')

const start = (container) => {
    return new Promise((resolve, reject) => {
        const {port} = container.resolve('serverSettings')
        const repo = container.resolve('repo')

        if (!repo) {
            reject(new Error('The server must be started with a connected repository'))
        }
        if (!port) {
            reject(new Error('The server must be started with an available port'))
        }

        const app = express()
        app.use(morgan('dev'))
        app.use(bodyparser.json())
        app.use(cors())
        app.use(helmet())
        app.use((err, req, res, next) => {
            reject(new Error('Something went wrong!, err:' + err))
            res.status(500).send('Something went wrong!')
            next()
        })
        app.use((req, res, next) => {
            req.container = container.createScope()
            next()
        })

        const api = _api.bind(null, {repo})
        api(app)

        const Eureka = require('eureka-js-client').Eureka;

        const eureka = new Eureka({
            // application instance information
            instance: {
                app: 'booking-service',
                hostName: 'localhost',
                ipAddr: '127.0.0.1',
                port: {
                '$': port,
                    '@enabled': true,
                },
                vipAddress: 'booking.test.something.com',
                dataCenterInfo: {
                    '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
                    name: 'MyOwn',
                },
            },
            eureka: {
                // eureka server host / port
                host: '127.0.0.1',
                port: 8080,
                servicePath: '/eureka-server-1.8.8/v2/apps'
            },
        });
        eureka.logger.level('debug');
        eureka.start(function (error) {
            console.log(error || 'complete');
        });
        const server = app.listen(port, () => resolve(server))

    })
}

module.exports = Object.assign({}, {start})
